;; -*- lexical-binding: t -*-

;;; soci-el --- ActivityStreams based social networking for Guile
;;; Copyright © 2016 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This file is part of soci-el.
;;;
;;; soci-el is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; soci-el is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with soci-el.  If not, see <http://www.gnu.org/licenses/>.

(defvar soci~generics
  (make-hash-table :test 'eq)
  "Registry for all the soci-el generic methods.")

(defun soci-put-method (funcname astype func)
  "Assign FUNC for FUNCNAME and ASTYPE in the generic methods registry"
  (let ((generic-table (or (gethash funcname soci~generics)
                           (let ((new-table (make-hash-table :test 'eq)))
                             (puthash funcname new-table soci~generics)
                             new-table))))
    (puthash astype func generic-table)
    t))

(defun soci-get-best-method (funcname astype-list)
  "Get the best/first available method for FUNCNAME looking in ASTYPE-LIST"
  (let ((generic-table (gethash funcname soci~generics)))
    (if (not generic-table)
        (error "No such generic \"%s\"" funcname))
    (catch 'found-it
      (mapc (lambda (astype)
              (let ((method (gethash astype generic-table)))
                (if method
                    (throw 'found-it method))))
            astype-list)
      (error "No method available for '%s and types %s"
             funcname (mapcar 'soci-astype-short-id astype-list)))))

(defun soci-method-apply (funcname asobj &rest args)
  "Look up best method for FUNCNAME + ASOBJ, apply with ASOBJ and ARGS"
  (apply (soci-get-best-method funcname (soci-asobj-inherits asobj))
         asobj args))

(defmacro soci-defgeneric (funcname &optional docstring)
  "Define generic method with FUNCNAME, and possibly assign DOCSTRING.

This is the entry point into a function with this name.  You'll need to
call this to call methods defined with `soci-defmethod'."
  (declare (indent defun))
  `(defun ,funcname (asobj &rest args)
     ,@(if docstring
           docstring '())
     (apply 'soci-method-apply (quote ,funcname) asobj args)))

(defmacro soci-defmethod (funcname args &rest body)
  "Define a method for FUNCNAME.

ARGS have a special shape, like
  ((asobj soci:Activity) args ...)
where soci:Activity may be replaced with any astype definition.

An example:

  (soci-defmethod list-astypes-exuberantly ((asobj soci:Object) say-this)
    (format \"%s! Look at all these types: %s\"
            say-this (soci-asobj-types-shortids asobj)))"
  (declare (indent defun))
  (let* ((asobj-arg (caar args))
         (astype (cadar args))
         (func-args (cons asobj-arg (cdr args))))
    `(soci-put-method (quote ,funcname) ,astype
                      (lambda ,func-args
                        ,@body))))
