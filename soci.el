;; -*- lexical-binding: t -*-

;;; soci-el --- ActivityStreams based social networking for Guile
;;; Copyright © 2016 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This file is part of soci-el.
;;;
;;; soci-el is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; soci-el is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with soci-el.  If not, see <http://www.gnu.org/licenses/>.


;;; Utilities
;;; =========

(defmacro soci-defun-promised-property (funcname prop calculate-func)
  (let ((typed-proplist (gensym)))
    `(defun ,funcname (,typed-proplist)
       (let ((existing-val  ; did we already calculate this?
              (soci-ptyped-member ,typed-proplist ,prop)))
         (if existing-val
             (cadr existing-val)  ; we already had it, so return that
           (progn
             (let ((val (funcall ,calculate-func ,typed-proplist)))
               (progn
                 (soci-ptyped-put ,typed-proplist ,prop val)
                 val))))))))

(defun soci-dedupe (lst)
  "Returns a (reversed) deduplication of LST"
  (let ((seen (make-hash-table))
        (final-lst '()))
    (mapc
     (lambda (astype)
       (if (not (gethash astype seen))
           (progn
             (puthash astype t seen)
             (setq final-lst
                   (cons astype final-lst)))))
     lst)
    final-lst))

;;; > Tagged property lists
;;; -----------------------

(defun soci-ptyped-type (ptyped)
  "Get type from typed property list"
  (car ptyped))

(defun soci-ptyped-type-p (x type-symbol)
  "Is X a typed property list of type TYPE-SYMBOL?"
  (and (consp x)
       (eq (car x) type-symbol)))

(defun soci-ptyped-get (ptyped prop)
  "Like `plist-get' for typed property list"
  (plist-get (cdr ptyped) prop))

(defun soci-ptyped-member (ptyped prop)
  "Like `plist-member' for typed property list"
  (plist-member (cdr ptyped) prop))

(defun soci-ptyped-put (ptyped prop val)
  "Like `plist-put' for typed property list"
  (let ((plist (cdr ptyped)))
    (if plist
        (plist-put (cdr ptyped) prop val)
      (setcdr ptyped (list prop val)))))



;;; Core datastructures
;;; ===================


;;; > asobj
;;; -------

(defun soci-asobj (json &rest props)
  "Construct an ActivityStreams object (asobj) from JSON

JSON may be a string or already parsed json-as-alist.  Additional
properties may be provided via PROPS."
  (let ((json (if (stringp json)
                  (json-read-from-string json)
                json)))
    (cons '*asobj*
          `(:json ,json ,@props))))

(defun soci-asobj-p (x)
  "Find out if X is an asobj."
  (soci-ptyped-type-p x '*asobj*))

(defun soci-asobj-json (asobj)
  "Get json from ASOBJ"
  (soci-ptyped-get asobj :json))

(soci-defun-promised-property
 soci-asobj-inherits
 :inherits 'soci-asobj-calculate-inherits)

(defun soci-asobj-calculate-inherits (asobj)
  (soci-astype-list-inheritance (soci-asobj-types asobj)))

(defun soci-alist-search-keylist (alist key-list &optional dflt)
  "Search a symbol ALIST for a chain of keys in KEY-LIST.

Use DFLT if we don't find it."
  (cond ((null key-list) dflt) ; must have been an empty key-list to start with
        ((null (cdr key-list)) ; reached the end, look here
         (assoc-default (car key-list) alist 'eq dflt))  ; find or return dflt
        (t (let* ((next-section (assoc (car key-list) alist))
                  (looks-like-alist
                   (and next-section
                        (consp (cdr next-section))
                        (consp (car (cdr next-section))))))
             (if looks-like-alist
                 (soci-alist-search-keylist (cdr next-section)
                                            (cdr key-list)
                                            dflt) ; keep searchinb
               dflt))))) ; no luck!

(defun soci-asobj-ref-raw (asobj key &optional dflt)
  "Like soci-asobj-ref, but don't automatically convert a result
to an asobj if it looks like we can."
  (if (consp key)
      (soci-alist-search-keylist (soci-asobj-json asobj)
                                 key dflt)
    (assoc-default key (soci-asobj-json asobj)
                   'eq dflt)))


(defun soci-asobj-convert-for-ref (item)
  (cond
   ;; Is it an asobj?  Return it wrapped as such
   ((and (consp item)
         (or (assoc 'type item)
             (assoc '@type item)))
    (soci-asobj item))
   ;; Convert json lists of possibly asobjs recursively
   ((vectorp item)
    (let ((new-vector (make-vector (length item) nil)))
      (dotimes (i (length item))
        (aset new-vector i
              (soci-asobj-convert-for-ref (aref item i))))
      new-vector))
   ;; otherwise return as-is
   (t item)))

(defun soci-asobj-ref (asobj key &optional dflt)
  "Get KEY from ASOBJ's json, or return DFLT.

KEY is possibly a list of keys, in which case the json structure will
be traversed recursively.

If the result looks like it's an asobj (it's an alist with a type
key) we'll wrap it in an asobj type."
  (let ((result (soci-asobj-ref-raw asobj key dflt)))
    (soci-asobj-convert-for-ref result)))

(defun soci-asobj-id (asobj)
  "Get the identifier for an asobj object.

Will look for first 'id and then '@id."
  (or (soci-asobj-ref asobj 'id)
      (soci-asobj-ref asobj '@id)))

(defun soci-asobj-type-field (asobj)
  "Get the type property for an asobj object.

Will look first for 'type and then '@type."
  (or (soci-asobj-ref asobj 'type)
      (soci-asobj-ref asobj '@type)))

(soci-defun-promised-property
 soci-asobj-types
 :inherits 'soci-asobj-calculate-types)

(defun soci-asobj-calculate-types (asobj)
  (let* ((types-from-sjson
          (soci-asobj-type-field asobj))
         (types-as-vector
          (if (vectorp types-from-sjson)
              types-from-sjson
            (vector types-from-sjson)))
         (types '()))
    (dotimes (i (length types-as-vector))
      (let* ((type-str (aref types-as-vector i))
             (astype (soci-asenv-type-by-str soci-default-asenv
                                             type-str)))
        (if astype
            (setq types (cons astype types)))))
    types))

(defun soci-asobj-types-shortids (asobj)
  (mapcar 'soci-astype-short-id (soci-asobj-types asobj)))



;;; > astype
;;; --------

(defun soci-astype (uri parents &optional short-id notes)
  (cons '*astype*
        `(:uri ,uri :parents ,parents :short-id ,short-id :notes ,notes)))

(defun soci-astype-uri (astype)
  "Get uri property from ASTYPE"
  (soci-ptyped-get astype :uri))

(defun soci-astype-parents (astype)
  "Get parents property from ASTYPE"
  (soci-ptyped-get astype :parents))

(defun soci-astype-short-id (astype)
  "Get short-id property from ASTYPE"
  (soci-ptyped-get astype :short-id))

(defun soci-astype-notes (astype)
  "Get notes property from ASTYPE"
  (soci-ptyped-get astype :notes))

(defun soci-astype-family-traverse (astype cur-family)
  (setq cur-family (cons astype cur-family))
  (mapc (lambda (parent)
          (setq cur-family
                (soci-astype-family-traverse
                 parent cur-family)))
        (soci-astype-parents astype))
  cur-family)

(defun soci-astype-list-inheritance (astype-list)
  "Calculate inheritance for a list of astypes"
  (let ((family '()))
    (mapc (lambda (astype)
            (setq family
                  (soci-astype-family-traverse astype family)))
          astype-list)
    (soci-dedupe family)))


;;; > asenv
;;; -------

(defun soci-asenv (vocab)
  "Build an asenv (vocabulary environment) from VOCABS"
  (let* ((uri-map (soci-build-astype-map vocab 'soci-astype-uri))
         (short-ids-map (soci-build-astype-map vocab 'soci-astype-short-id)))
    (cons '*asenv*
          `(:vocab ,vocab :uri-map ,uri-map
            :short-ids-map ,short-ids-map))))

(defun soci-asenv-vocab (asenv)
  "Get vocab property from ASENV"
  (soci-ptyped-get asenv :vocab))

(defun soci-asenv-uri-map (asenv)
  "Get uri-map property from ASENV"
  (soci-ptyped-get asenv :uri-map))

(defun soci-asenv-short-ids-map (asenv)
  "Get short-ids property from ASENV"
  (soci-ptyped-get asenv :short-ids-map))


(defun soci-asenv-type-by-str (asenv type-str)
  "Try to get a type from ASENV looking up TYPE-STR"  ; *without* expanding
  ;; @@: We might never have json-ld support anyway... hence commenting out
  ;;   "*without* expanding" from the docstring
  (or (soci-asenv-type-by-short-id asenv type-str)
      (soci-asenv-type-by-uri asenv type-str)))

(defun soci-asenv-type-by-short-id (asenv type-str)
  (gethash type-str (soci-asenv-short-ids-map asenv)))

(defun soci-asenv-type-by-uri (asenv type-str)
  (gethash type-str (soci-asenv-uri-map asenv)))

(defun soci-build-astype-map (vocab get-key)
  "Build a uri map (a hash table) from VOCABS"
  (let ((uri-map (make-hash-table :test 'equal)))
    (mapc (lambda (astype)
            (let ((key (funcall get-key astype)))
              (if key
                  (puthash key astype uri-map))))
          vocab)
    uri-map))

(defun soci-asenv-p (x)
  "Check if X is an asenv."
  (and (consp x)
       (eq (car x) '*asenv*)))



;;; Vocabulary
;;; ==========

(defun soci-as-uri (identifier)
  (concat "http://www.w3.org/ns/activitystreams#"
          identifier))


;;; > Core classes
;;; --------------

(setq soci:Object
  (soci-astype
   (soci-as-uri "Object") '() "Object"
   "Describes an object of any kind.
The Object class serves as the base class for most of the
other kinds of objects defined in the Activity Vocabulary,
include other Core classes such as Activity,
IntransitiveActivity, Actor, Collection and OrderedCollection."))

(setq soci:Link
  (soci-astype
   (soci-as-uri "Link") '() "Link"
   "A Link is an indirect, qualified reference to a resource identified by
a URL. The fundamental model for links is established by [RFC5988].
Many of the properties defined by the Activity Vocabulary allow
values that are either instances of Object or Link. When a Link is
used, it establishes a qualified relation connecting the subject (the
containing object) to the resource identified by the href."))

(setq soci:Activity
  (soci-astype
   (soci-as-uri "Activity") (list soci:Object) "Activity"
   "An Activity is a subclass of Object that describes some form of
action that may happen, is currently happening, or has already
happened. The Activity class itself serves as an abstract base
class for all types of activities. It is important to note that
the Activity class itself does not carry any specific semantics
about the kind of action being taken."))

(setq soci:IntransitiveActivity
  (soci-astype
   (soci-as-uri "IntransitiveActivity") (list soci:Activity) "IntransitiveActivity"
   "Instances of IntransitiveActivity are a subclass of Activity whose
actor property identifies the direct object of the action as opposed
to using the object property."))

(setq soci:Collection
  (soci-astype
   (soci-as-uri "Collection") (list soci:Object) "Collection"
   "A Collection is a subclass of Object that represents ordered or
unordered sets of Object or Link instances.

Refer to the Activity Streams 2.0 Core specification for a complete
description of the Collection type."))

(setq soci:OrderedCollection
  (soci-astype
   (soci-as-uri "OrderedCollection") (list soci:Collection) "OrderedCollection"
   "A subclass of Collection in which members of the logical collection
are assumed to always be strictly ordered."))

(setq soci:CollectionPage
  (soci-astype
   (soci-as-uri "CollectionPage") (list soci:Collection) "CollectionPage"
   "Used to represent distinct subsets of items from a Collection.
Refer to the Activity Streams 2.0 Core for a complete description of
the CollectionPage object."))

(setq soci:OrderedCollectionPage
  (soci-astype
   (soci-as-uri "OrderedCollectionPage") (list soci:OrderedCollection soci:CollectionPage)
   "OrderedCollectionPage"
   "Used to represent ordered subsets of items from an OrderedCollection.
Refer to the Activity Streams 2.0 Core for a complete description of
the OrderedCollectionPage object."))



;;; > Extended Classes: Activity Types
;;; ----------------------------------

(setq soci:Accept
  (soci-astype
   (soci-as-uri "Accept") (list soci:Activity) "Accept"
   "Indicates that the actor accepts the object.
The target property can be used in certain circumstances to indicate
the context into which the object has been accepted. For instance,
when expressing the activity, \"Sally accepted Joe into the Club\",
the \"target\" would identify the \"Club\"."))

(setq soci:TentativeAccept
  (soci-astype
   (soci-as-uri "TentativeAccept") (list soci:Accept) "TentativeAccept"
   "A specialization of Accept indicating that the acceptance is tentative."))

(setq soci:Add
  (soci-astype
   (soci-as-uri "Add") (list soci:Activity) "Add"
   "Indicates that the actor has added the object to the target. If the
target property is not explicitly specified, the target would need
to be determined implicitly by context. The origin can be used to
identify the context from which the object originated."))

(setq soci:Arrive
  (soci-astype
   (soci-as-uri "Arrive") (list soci:IntransitiveActivity) "Arrive"
   "An IntransitiveActivity that indicates that the actor has arrived
at the location. The origin can be used to identify the context
from which the actor originated. The target typically has no defined
meaning."))

(setq soci:Create
  (soci-astype
   (soci-as-uri "Create") (list soci:Activity) "Create"
   "Indicates that the actor has created the object."))

(setq soci:Delete
  (soci-astype
   (soci-as-uri "Delete") (list soci:Activity) "Delete"
   "Indicates that the actor has deleted the object. If specified,
the origin indicates the context from which the object was
deleted."))

(setq soci:Follow
  (soci-astype
   (soci-as-uri "Follow") (list soci:Activity) "Follow"
   "Indicates that the actor is \"following\" the object. Following is
defined in the sense typically used within Social systems in which
the actor is interested in any activity performed by or on the
object. The target and origin typically have no defined meaning."))

(setq soci:Ignore
  (soci-astype
   (soci-as-uri "Ignore") (list soci:Activity) "Ignore"
   "Indicates that the actor is ignoring the object.
The target and origin typically have no defined meaning."))

(setq soci:Join
  (soci-astype
   (soci-as-uri "Join") (list soci:Activity) "Join"
   "Indicates that the actor has joined the object. The target and
origin typically have no defined meaning."))

(setq soci:Leave
  (soci-astype
   (soci-as-uri "Leave") (list soci:Activity) "Leave"
   "Indicates that the actor has left the object. The target and origin
typically have no meaning."))

(setq soci:Like
  (soci-astype
   (soci-as-uri "Like") (list soci:Activity) "Like"
   "Indicates that the actor likes, recommends or endorses the object.
The target and origin typically have no defined meaning."))

(setq soci:Offer
  (soci-astype
   (soci-as-uri "Offer") (list soci:Activity) "Offer"
   "Indicates that the actor is offering the object. If specified, the
target indicates the entity to which the object is being offered."))

(setq soci:Invite
  (soci-astype
   (soci-as-uri "Invite") (list soci:Offer) "Invite"
   "A specialization of Offer in which the actor is extending an
invitation for the object to the target."))

(setq soci:Reject
  (soci-astype
   (soci-as-uri "Reject") (list soci:Activity) "Reject"
   "Indicates that the actor is rejecting the object. The target and
origin typically have no defined meaning."))

(setq soci:TentativeReject
  (soci-astype
   (soci-as-uri "TentativeReject") (list soci:Reject) "TentativeReject"
   "A specialization of Reject in which the rejection is considered
tentative."))

(setq soci:Remove
  (soci-astype
   (soci-as-uri "Remove") (list soci:Activity) "Remove"
   "Indicates that the actor is removing the object. If specified, the
origin indicates the context from which the object is being removed."))

(setq soci:Undo
  (soci-astype
   (soci-as-uri "Undo") (list soci:Activity) "Undo"
   "Indicates that the actor is undoing the object. In most cases,
the object will be an Activity describing some previously performed
action (for instance, a person may have previously \"liked\"
an article but, for whatever reason, might choose to undo that
like at some later point in time).

The target and origin typically have no defined meaning."))

(setq soci:Update
  (soci-astype
   (soci-as-uri "Update") (list soci:Activity) "Update"
   "Indicates that the actor has updated the object. Note, however, that
this vocabulary does not define a mechanism for describing the
actual set of modifications made to object.

The target and origin typically have no defined meaning."))

(setq soci:Experience
  (soci-astype
   (soci-as-uri "Experience") (list soci:Activity) "Experience"
   "Indicates that the actor has experienced the object. The type of
experience is not specified."))

(setq soci:View
  (soci-astype
   (soci-as-uri "View") (list soci:Experience) "View"
   "Indicates that the actor has viewed the object. Viewing is a
specialization of Experience."))

(setq soci:Listen
  (soci-astype
   (soci-as-uri "Listen") (list soci:Experience) "Listen"
   "Indicates that the actor has listened to the object. Listening is a
specialization of Experience."))

(setq soci:Read
  (soci-astype
   (soci-as-uri "Read") (list soci:Experience) "Read"
   "Indicates that the actor has read the object. Reading is a
specialization of Experience."))

(setq soci:Move
  (soci-astype
   (soci-as-uri "Move") (list soci:Activity) "Move"
   "Indicates that the actor has moved object from origin to target. If
the origin or target are not specified, either can be determined by
context."))

(setq soci:Travel
  (soci-astype
   (soci-as-uri "Travel") (list soci:IntransitiveActivity) "Travel"
   "Indicates that the actor is traveling to target from origin.
Travel is an IntransitiveObject whose actor specifies the direct
object. If the target or origin are not specified, either can be
determined by context."))

(setq soci:Announce
  (soci-astype
   (soci-as-uri "Announce") (list soci:Activity) "Announce"
   "Indicates that the actor is calling the target's attention the object.

The origin typically has no defined meaning."))

(setq soci:Block
  (soci-astype
   (soci-as-uri "Block") (list soci:Ignore) "Block"
   "Indicates that the actor is blocking the object. Blocking is a
stronger form of Ignore. The typical use is to support social systems
that allow one user to block activities or content of other users.

The target and origin typically have no defined meaning."))

(setq soci:Flag
  (soci-astype
   (soci-as-uri "Flag") (list soci:Activity) "Flag"
   "Indicates that the actor is \"flagging\" the object. Flagging is
defined in the sense common to many social platforms as reporting
content as being inappropriate for any number of reasons."))

(setq soci:Dislike
  (soci-astype
   (soci-as-uri "Dislike") (list soci:Activity) "Dislike"
   "Indicates that the actor dislikes the object."))


;;; > Extended Classes: Actor types
;;; -------------------------------

(setq soci:Application
  (soci-astype
   (soci-as-uri "Application") (list soci:Object) "Application"
   "Describes a software application."))

(setq soci:Group
  (soci-astype
   (soci-as-uri "Group") (list soci:Object) "Group"
   "Represents a formal or informal collective of Actors."))

(setq soci:Organization
  (soci-astype
   (soci-as-uri "Organization") (list soci:Object) "Organization"
   "Represents an organization."))

(setq soci:Person
  (soci-astype
   (soci-as-uri "Person") (list soci:Object) "Person"
   "Represents an individual person."))

(setq soci:Service
  (soci-astype
   (soci-as-uri "Service") (list soci:Object) "Service"
   "Represents a service of any kind."))



;;; > Relationship
;;; --------------

(setq soci:Relationship
  (soci-astype
   (soci-as-uri "Relationship") (list soci:Object) "Relationship"
   "Describes a relationship between two individuals.
The subject and object properties are used to identify the
connected individuals.

See 3.3.1 [of ActivityStreams 2.0 Vocabulary document] Representing
Relationships Between Entities for additional information."))

(setq soci:Content
  (soci-astype
   (soci-as-uri "Content") (list soci:Object) "Content"
   "Describes an entity representing any form of content. Examples
include documents, images, etc. Content objects typically are not
able to perform activities on their own, yet rather are usually the
object or target of activities."))

(setq soci:Article
  (soci-astype
   (soci-as-uri "Article") (list soci:Content) "Article"
   "Represents any kind of multi-paragraph written work."))

(setq soci:Album
  (soci-astype
   (soci-as-uri "Album") (list soci:Collection) "Album"
   "A type of Collection typically used to organize Image, Video or Audio
objects."))

(setq soci:Folder
  (soci-astype
   (soci-as-uri "Folder") (list soci:Collection) "Folder"
   "A type of Collection typically used to organize objects such as
Documents."))

(setq soci:Story
  (soci-astype
   (soci-as-uri "Story") (list soci:OrderedCollection) "Story"
   "A type of Ordered Collection usually containing Content Items
organized to \"tell a story\"."))

(setq soci:Document
  (soci-astype
   (soci-as-uri "Document") (list soci:Content) "Document"
   "Represents a document of any kind."))

(setq soci:Audio
  (soci-astype
   (soci-as-uri "Audio") (list soci:Document) "Audio"
   "Represents an audio document of any kind."))

(setq soci:Image
  (soci-astype
   (soci-as-uri "Image") (list soci:Document) "Image"
   "An image document of any kind."))

(setq soci:Video
  (soci-astype
   (soci-as-uri "Video") (list soci:Document) "Video"
   "Represents a video document of any kind."))

(setq soci:Note
  (soci-astype
   (soci-as-uri "Note") (list soci:Object) "Note"
   "Represents a short work typically less than a single
paragraph in length."))

(setq soci:Page
  (soci-astype
   (soci-as-uri "Page") (list soci:Document) "Page"
   "Represents a Web Page."))

(setq soci:Question
  (soci-astype
   (soci-as-uri "Question") (list soci:Content soci:IntransitiveActivity) "Question"
   "Represents a question being asked. Question objects are unique in
that they are an extension of both Content and IntransitiveActivity.
That is, the Question object is an Activity but the direct object is
the question itself."))

(setq soci:Event
  (soci-astype
   (soci-as-uri "Event") (list soci:Object) "Event"
   "Represents any kind of event."))

(setq soci:Place
  (soci-astype
   (soci-as-uri "Place") (list soci:Object) "Place"
   "Represents a logical or physical location.
See 3.3.2 Representing Places [of ActivityStreams 2.0 Vocabulary
document] for additional information."))

(setq soci:Mention
  (soci-astype
   (soci-as-uri "Mention") (list soci:Link) "Mention"
   "A specialized Link that represents an @mention."))

(setq soci:Profile
  (soci-astype
   (soci-as-uri "Profile") (list soci:Content) "Profile"
   "A Profile is a content object that describes another Object,
typically used to describe Actor, objects. The describes property
is used to reference the object being described by the profile."))

(defvar soci-core-vocab
  (list soci:Object soci:Link soci:Activity soci:IntransitiveActivity
        soci:Collection soci:OrderedCollection soci:CollectionPage
        soci:OrderedCollectionPage soci:Accept soci:TentativeAccept soci:Add
        soci:Arrive soci:Create soci:Delete soci:Follow soci:Ignore
        soci:Join soci:Leave soci:Like soci:Offer soci:Invite soci:Reject
        soci:TentativeReject soci:Remove soci:Undo soci:Update
        soci:Experience soci:View soci:Listen soci:Read soci:Move
        soci:Travel soci:Announce soci:Block soci:Flag soci:Dislike
        soci:Application soci:Group soci:Organization soci:Person
        soci:Service soci:Relationship soci:Content soci:Article soci:Album
        soci:Folder soci:Story soci:Document soci:Audio soci:Image
        soci:Video soci:Note soci:Page soci:Question soci:Event soci:Place
        soci:Mention soci:Profile)
  "Core ActivityStreams vocabulary.")

(defcustom soci-default-asenv
  (soci-asenv soci-core-vocab)
  "Default vocabulary environment"
  :type '(restricted-sexp :match-alternatives
                          (soci-asenv-p)))
