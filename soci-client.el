;; -*- lexical-binding: t -*-

;;; soci-el --- ActivityStreams based social networking for Guile
;;; Copyright © 2016 Christopher Allan Webber <cwebber@dustycloud.org>
;;;
;;; This file is part of soci-el.
;;;
;;; soci-el is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; soci-el is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with soci-el.  If not, see <http://www.gnu.org/licenses/>.

(defvar soci-client-default-width 79)


;;; Rendering stuff
;;; ===============

(defun soci-print-header (name val)
  (let ((start (point)))
    (insert name) (insert ": ")
    (add-face-text-property start (point) 'message-header-name))
  (insert val)
  (newline))

(defun soci-print-separator (&optional char width face)
  "Print CHAR (really a string) WIDTH times, and add a newline

Defaults to \"=\" and 80, respectively."
  (let ((char (or char "="))
        (width (or width soci-client-default-width))
        (start (point)))
    (dotimes (i width)
      (insert char))
    (newline)
    (if face
        (save-excursion
          (previous-line)
          (end-of-line)
          (add-face-text-property start (point) face)))))

(defun soci-insert-bracketed-link (url &optional text title)
  "Insert bracketed link into the buffer"
  (insert "[")
  (let ((str-start (point)))
    (insert (or text url))
    (shr-urlify str-start url title))
  (insert "]"))

(defun soci-bracketed-link (url &optional text title)
  "Return bracketed link as a (marked up string)"
  (with-temp-buffer
    (soci-insert-bracketed-link url text title)
    (buffer-string)))

(soci-defgeneric soci-asobj-render-toplevel
  "Render a top-level activity")

(soci-defmethod soci-asobj-render-toplevel ((asobj soci:Create))
  ;; Insert top header
  (soci-print-separator "=" nil 'magit-diff-hunk-heading)
  ;; insert subject field
  (let ((start (point)))
    (insert "+> ")
    (soci-insert-bracketed-link (or (soci-asobj-ref asobj '(object uri))
                                    (soci-asobj-ref asobj '(object id))
                                    (soci-asobj-ref asobj '(object @id))
                                    (soci-asobj-ref asobj 'uri)
                                    (soci-asobj-id asobj))
                                (or (soci-asobj-ref asobj '(object name))
                                    (soci-asobj-ref asobj 'name)
                                    nil))
    (add-face-text-property start (point) 'outline-1 t))
  (newline)
  ;; Headers
  (let* ((actor (or (soci-asobj-ref asobj '(object attributedTo))  ; TODO: distinguish these...
                    (soci-asobj-ref asobj 'actor)))
         (published (or (soci-asobj-ref asobj '(object published))
                        (soci-asobj-ref asobj 'published))))
    (when actor
      (soci-print-header "By"
                         (soci-bracketed-link
                          (or (soci-asobj-ref actor 'uri)
                              (soci-asobj-ref actor 'id)
                              (soci-asobj-ref actor '@id))
                          (or (soci-asobj-ref actor 'name)
                              (soci-asobj-ref actor 'displayName)
                              "???"))))
    (when published
      (soci-print-header "At" published)))
  (soci-print-separator "-" nil 'magit-diff-context-highlight)
  (newline)
  (soci-render-object-body (soci-asobj-ref asobj 'object)))

(soci-defmethod soci-asobj-render-toplevel ((asobj soci:Tombstone))
  ;; Insert top header
  (soci-print-separator "=" nil 'magit-diff-hunk-heading)
  (let ((deleted (soci-asobj-ref asobj "deleted")))
    (if deleted
        (insert (concat "** Deleted on " deleted "! **"))
      (insert "** Deleted! **"))))

(soci-defmethod soci-asobj-render-toplevel ((asobj soci:Activity))
  ;; TODO: Print a warning into the UI of "<<Unknown activity type>>"
  'TODO)

(soci-defmethod soci-asobj-render-toplevel ((asobj soci:Object))
  ;; If it's not an Activity, and just an Object, we'll wrap in a Create
  ;; ourselves, copying some info over from the other activity
  (let ((new-asobj (soci-asobj `((type . "Create")
                                 (object . ,(soci-asobj-json asobj))))))
    (soci-asobj-render-toplevel new-asobj)))

(soci-defgeneric soci-render-object-body
  "Render the body of a common post, esp a Create")

(defun soci-render-object-content (asobj)
  (let ((content (soci-asobj-ref asobj 'content))
        (initial-pos (point))
        (shr-width soci-client-default-width)) 
    (when content
      ;; TODO: insert as HTML
      (insert content)
      (shr-render-region initial-pos (point)))))

(soci-defmethod soci-render-object-body ((asobj soci:Object))
  (soci-render-object-content asobj))

(soci-defmethod soci-render-object-body ((asobj soci:Image))
  (let ((images (soci-asobj-ref asobj 'url)))
    (dotimes (i (length images))
      (let ((image (aref images i)))
        (insert " - ")
        (soci-insert-bracketed-link
         (soci-asobj-ref image 'href)
         (or (soci-asobj-ref image 'name)
             (soci-asobj-ref image 'href)))
        (newline)))
    (newline)
    (soci-render-object-content asobj)))

(soci-defmethod soci-render-object-body ((asobj soci:Tombstone))
  (let ((deleted (soci-asobj-ref asobj "deleted")))
    (if deleted
        (insert (concat "** Deleted on " deleted "! **"))
      (insert "** Deleted! **"))))


;;; URL fetching and etc
;;; ====================

;; TODO: Permit banning localhost and arbitrary things

(defun soci-url-safe-p (url)
  "Predicate checking whether URL is safe to fetch"
  (member (url-type (url-generic-parse-url url)) '("http" "https")))

(defun soci-ensure-url-safe (url)
  "Make sure that the URL is safe to fetch.  If not, throw an error."
  (if (not (soci-url-safe-p url))
      (error "Unsafe URL type for %s" url)))

;; TODO: we'll want to permit passing in a bearer token, maybe more
(defun soci-fetch-asobj (url &optional extra-headers)
  "Fetch URL which we can presumably access"
  (soci-ensure-url-safe url)
  (let* ((url-mime-accept-string "application/activity+json")
         (url-request-extra-headers (or extra-headers
                                        url-request-extra-headers))
         (result-buffer (url-retrieve-synchronously url))
         (mm-dissected
          (save-current-buffer
            (set-buffer result-buffer)
            ;; (buffer-string)
            (mm-dissect-buffer t t)))
         (asobj (if (and mm-dissected
                         (equal (car (mm-handle-type mm-dissected))
                                "application/activity+json"))
                    (soci-asobj (save-current-buffer
                                  (set-buffer (mm-handle-buffer mm-dissected))
                                  (buffer-string)))
                  nil)))
    ;; Clean up
    (kill-buffer result-buffer)
    (if mm-dissected
        (kill-buffer (mm-handle-buffer mm-dissected)))
    ;; return asobj
    asobj))


(defun soci-url-encode-pair (pair)
  (concat (url-hexify-string (car pair))
          "="
          (url-hexify-string (cdr pair))))

(defun soci-url-encode-params (params)
  "Return a url-encoded string from alist of parameters PARAMS"
  (let ((result "")
        (first t))
    (mapc (lambda (pair)
            (if first
                (progn
                 (setq first nil)
                 (setq result
                       (soci-url-encode-pair pair)))
              (setq result
                    (concat result "&"
                            (soci-url-encode-pair pair)))))
          params)
    result))


;;; ActivityPub client & interactions
;;; =================================

(defun soci-client (id &rest other-args)
  (cons '*soci-client*
        `(:id ,id ,@other-args)))

(defvar soci-default-client nil)

(defun soci-read-till-safe-url (prompt)
  (let ((first-try t)
        (valid nil)
        (result nil))
    (while (not valid)
      (setq result
            (read-from-minibuffer
             (concat
              (if first-try
                  prompt
                "Sorry, please provide an https/http URI: "))))
      (setq valid (soci-url-safe-p result))
      (setq first-try nil))
    result))

(defun soci-setup-client ()
  "Set up soci-default-client"
  (interactive)
  ;; Set up the start of the client, including the id
  (if (not soci-default-client)
      (setq soci-default-client
            (soci-client (soci-read-till-safe-url
                          "URL of your ActivityPub user: "))))
  (let ((user-asobj (soci-fetch-asobj
                     (soci-ptyped-get soci-default-client :id))))
    (soci-ptyped-put soci-default-client :asobj user-asobj)
    (when (not (soci-ptyped-get soci-default-client :auth-token))
      (let ((auth-token-endpoint (soci-asobj-ref user-asobj
                                                 '(endpoints getAuthToken))))
        (if (not auth-token-endpoint)
            (error "ActivityPub API: No getAuthToken endpoint set on user!"))
        (browse-url-default-browser auth-token-endpoint)
        (soci-ptyped-put soci-default-client :auth-token
                         (read-from-minibuffer
                          "Get token from browser, paste here: ")))))
  (message "soci user set up!"))

(defun soci-render-outbox (&optional outbox-uri)
  "Render all the cool things you've said lately"
  (interactive)
  (let* ((outbox-uri (or outbox-uri (soci-asobj-ref
                                     (soci-ptyped-get soci-default-client :asobj)
                                     'outbox)))
         (as-items (soci-asobj-ref
                    (soci-fetch-asobj outbox-uri)
                    '(first orderedItems)))
         (buf (generate-new-buffer "*soci-outbox*")))
    (switch-to-buffer buf)
    (dotimes (i (length as-items))
      (let ((asobj (aref as-items i)))
        ;; Insert top header
        (soci-print-separator "=" nil 'magit-diff-hunk-heading)
        (soci-asobj-render-toplevel asobj)))))

(defun soci-render-inbox (&optional inbox-uri)
  "Render all your recent info"
  (interactive)
  (let* ((inbox-uri (or inbox-uri (soci-asobj-ref
                                   (soci-ptyped-get soci-default-client :asobj)
                                   'inbox)))
         (bearer-token (concat "Bearer "
                               (soci-ptyped-get soci-default-client
                                                :auth-token)))
         (as-items (soci-asobj-ref
                    (soci-fetch-asobj inbox-uri
                                      `(("Authorization" . ,bearer-token)))
                    '(first orderedItems)))
         (buf (generate-new-buffer "*soci-inbox*")))
    (switch-to-buffer buf)
    (dotimes (i (length as-items))
      (let ((asobj (aref as-items i)))
        (soci-asobj-render-toplevel asobj)))))

(defun soci-compose ()
  "Compose a new message to post to your soci-el netwurk"
  (interactive)
  (let ((buf (generate-new-buffer "*soci-compose*"))
        (headers '("To" "Cc" "Bcc" "Subject")))
    (switch-to-buffer buf)
    (org-mode)
    (mapcar (lambda (header)
              (let ((start (point)))
                (insert "# ")
                (insert header)
                (insert ":")
                (add-face-text-property start (point) 'message-header-name)
                ;; (put-text-property start (- (point) 1) 'read-only t)
                (insert " ")
                (newline)))
            headers)
    (let ((start (point)))
      (insert "# ")
      (insert mail-header-separator)
      (add-face-text-property start (point) 'mu4e-compose-separator-face)
      (newline)
      (put-text-property start (- (point) 1) 'read-only t))))

(defun soci-get-separator-re ()
  (concat "^#[[:space:]]+" mail-header-separator "$"))

(defvar soci-header-re
  "^# +\\([[:alnum:]]+\\): +?\\(.*?\\)\n")

(defun soci-compose-get-headers ()
  (save-excursion
    (goto-char (point-min))
    (let ((separator-re (soci-get-separator-re))
          (headers-alist '()))
      (while (not (or (looking-at separator-re)
                      (equal (point) (point-max))))
        (when (looking-at soci-header-re)
          (setq headers-alist
                (cons (cons (match-string 1) (match-string 2))
                      headers-alist)))
        (forward-line))
      headers-alist)))

(defun soci-compose-get-body ()
  (save-excursion
    (goto-char (point-min))
    (re-search-forward (soci-get-separator-re))
    (buffer-substring (+ (point) 1) (point-max))))

;; @@: This is a terrible kludge, especially in the way it abuses
;;     the region...
;;     Better exporting code is welcome :)
(defun soci-compose-get-htmlized-body ()
  (let ((body (soci-compose-get-body)))
    (with-temp-buffer
      (org-mode)
      (set-mark (point))
      (insert body)
      (goto-char (point-max))
      (let ((org-export-with-toc nil))
        (org-html-convert-region-to-html))
      (buffer-string))))

(defun soci-parse-recipients (recipients-line)
  (if recipients-line
      (apply 'vector (split-string recipients-line
                                   "," t " +"))
    nil))

(defun soci-compose-asjson ()
  "Construct activitystreams json from the current compose buffer

Note: does not wrap in an ASOBJ!"
  (let* ((headers (soci-compose-get-headers))
         (body-html (soci-compose-get-htmlized-body))
         (to (soci-parse-recipients
              (assoc-default "To" headers)))
         (cc (soci-parse-recipients
              (assoc-default "Cc" headers)))
         (bcc (soci-parse-recipients
               (assoc-default "Bcc" headers)))
         (subject (assoc-default "Subject" headers))
         (recipients `(,@(if to
                             (list (cons "to" to))
                           '())
                       ,@(if cc
                             (list (cons "cc" cc))
                           '())
                       ,@(if bcc
                             (list (cons "bcc" bcc))
                           '())))
         (note-asobj `((type . "Note")
                       ,@recipients
                       ,@(if subject
                             (list (cons "name" subject))
                           '())
                       (content . ,body-html))))
    `((type . "Create")
      ,@recipients
      (object . ,note-asobj))))


;; @@: Very WIP...
(defun soci-compose-post-to-outbox ()
  "Post the current buffer to the user's outbox."
  (interactive)
  (let* ((url-request-method "POST")
         (bearer-token (concat "Bearer "
                               (soci-ptyped-get soci-default-client
                                                :auth-token)))
         (url-request-extra-headers
          `(("Authorization" . ,bearer-token)
            ("Content-Type" . "application/activity+json; charset=utf-8")))
         (actor-asobj (soci-ptyped-get soci-default-client :asobj))
         (url-request-data
          ;; AAAAAAA! We're still getting errors here with multibyte stuff
          ;; and non-ascii chars and I don't know why!
          (string-to-unibyte
           (encode-coding-string (json-encode (soci-compose-asjson))
                                 'utf-8)))
         (outbox (soci-asobj-ref actor-asobj 'outbox))
         (result-buf (url-retrieve-synchronously outbox)))
    (save-current-buffer
      (set-buffer result-buf)
      (buffer-string))))

(defun soci-subscribe-to-user ()
  (interactive)
  (let* ((subscribe-to (soci-read-till-safe-url
                        "URL of user to subscribe to: "))
         (post-json
          `((type . "Follow")
            (object . ,subscribe-to)))
         (url-request-data
          (json-encode post-json))
         ;; here to the end could probably be abstracted...
         (url-request-method "POST")
         (bearer-token (concat "Bearer "
                               (soci-ptyped-get soci-default-client
                                                :auth-token)))
         (url-request-extra-headers
          `(("Authorization" . ,bearer-token)
            ("Content-Type" . "application/activity+json; charset=utf-8")))
         (actor-asobj (soci-ptyped-get soci-default-client :asobj))
         (outbox (soci-asobj-ref actor-asobj 'outbox))
         (result-buf (url-retrieve-synchronously outbox)))
    (save-current-buffer
      (set-buffer result-buf)
      (buffer-string))))
